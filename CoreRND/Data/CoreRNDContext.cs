﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace CoreRND.Models
{
    public class CoreRNDContext : DbContext
    {
        public CoreRNDContext (DbContextOptions<CoreRNDContext> options)
            : base(options)
        {
        }

        public DbSet<CoreRND.Models.Movie> Movie { get; set; }
    }
}
